let arrNamesLowerCase = ['Yarick', 'Vanya', 'Tanya', 'lena'];
let newArr = ['Vanya', 'Tanya', 'lena'];
let newArr22 = ['Ivan', 'Sony', 'Potap', 'rafick', 'Ramzez', 'bill'];
let namesLowerCase = (arr, val) => {
    let leng = arr.length;
    for (let i = 0; i < leng; i++) {
      arr[i] = val(arr[i]);
    }
    return arr;
  }
console.log(namesLowerCase(newArr, val => val.toLowerCase()));
console.log(namesLowerCase(arrNamesLowerCase, val => val.toLowerCase()));
console.log(namesLowerCase(newArr22, val => val.toLowerCase()));

let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function AscDescSort (arr, num) {
  let leng = arr.length;
  for (let k = 0; k < leng; k++) {
    let b = 0;
    for (let l = 0; l < leng; l++) {
      if (num(arr[k], arr[l])) {
        b = arr[l];
        arr[l] = arr[k];
        arr[k] = b;
      }
    }
  }
  return arr;
}
console.log(AscDescSort(mass, (k, l) => k < l));

class User {
   constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty) {
       this.firstName = firstName;
       this.secondName = secondName;
       this.lastName = lastName;
       this.age = age;
       this.receiptDate = receiptDate;
       this.dateOfIssue = dateOfIssue;
       this.specialty = specialty;
   }
   getShortName() {
       return [this.lastName, this.firstName.charAt(0) + '.', this.secondName.charAt(0) + '.'].map(letter => letter.charAt(0).toUpperCase() + letter.slice(1)).join(' ');
   }
   getFullName() {
       return [this.lastName, this.firstName, this.secondName].map(letter => letter.charAt(0).toUpperCase() + letter.slice(1)).join(' ');
   }
   getEducationYears() {
       return `${this.dateOfIssue - this.receiptDate} года ты учишься`;
   }
   getEducationMonths() {
       return `${(this.dateOfIssue - this.receiptDate) * 12} месяцев ты учишься`;
   }
   getAge() {
       return `${this.age} года`;
   }
   getOpportunityToHaveDiplom() {
        return this.dateOfIssue - this.receiptDate < 4 ? 'У тебя не может быть диплома' : 'У тебя может быть диплом'
    }
}
class Student extends User {
   constructor(grade) {
       super(grade);
       this.grade = grade;
   }
   getYourDimlom() {
       return this.grade >= 60 && this.grade <= 100 ? 'Поздровляю у тебя есть диплом' : 'Попробуй еще раз через год'
   }
}
class PreparGroup extends User {
   issueOrDeduction() {
       return false;
   }
}

let user = new User('yaroslav', 'evgenovich', 'resnenko', 22, 2016, 2020, 'Telecommunications and Radioelectronics');
console.log(user.getShortName());
console.log(user.getFullName());
console.log(user.getEducationYears());
console.log(user.getEducationMonths());
console.log(user.getAge());
console.log(user.getOpportunityToHaveDiplom());
let studentGrade = new Student(96);
console.log(studentGrade.getYourDimlom());
let group = new PreparGroup();
console.log(group.issueOrDeduction());